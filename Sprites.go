package main

import (
	"image"
	"image/color"
	_ "image/png"
	"log"
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	_ "github.com/hajimehoshi/ebiten/v2/examples/resources/images"
)


func (g *Game) UpdateSpriteRotations() {
	/*
	1. get vec towards camera
	camera.pos - obj.pos
	2. get angle of obj to the plane 
	atan2 gets this
	3. mul y by -1 because y is draw top to down
	*/
	// log.Print(
	// 	g.worldObjects[0].bounds.x2 - g.worldObjects[0].bounds.x1,
	// 	g.worldObjects[0].bounds.y2 - g.worldObjects[0].bounds.y1,
	// )
	return

	var dirToCamX, dirToCamY, radAngle1, radAngle2  float64
	var midX, midY  float64 //, x1, x2, y1, y2
	var x1, x2, y1, y2 float64 //, 
	for idx, _ := range g.worldObjects {
		
		x1 = g.worldObjects[idx].bounds.x1
		y1 = g.worldObjects[idx].bounds.y1
		x2 = g.worldObjects[idx].bounds.x2
		y2 = g.worldObjects[idx].bounds.y2
		
		midX = ((x1 + x2) / 2)
		midY = ((y1 + y2) / 2)
		
		dirToCamX = midX - g.player.px
		dirToCamY = midY - g.player.py
		radAngle1 = math.Atan2(dirToCamY, dirToCamX) + DEG_TO_RAD(180)
		
		_ = radAngle2
		
		x1 = x1 - midX 
		x2 = x2 - midX 
		
		y1 = y1 - midY
		y2 = y2 - midY
		
		radAngle2 = math.Atan2(
			(y2 - y1)*g.player.px - (x2 - x1)*g.player.py,
			g.player.px*(x2 - x1) + g.player.py*(y2 - y1),
		) + DEG_TO_RAD(180)
		
		radAngle1 = (radAngle1 - radAngle2) + DEG_TO_RAD(45)
		
		g.worldObjects[idx].bounds.x1 = ((x1 * SuperCos(radAngle1) - y1 * SuperSin(radAngle1))) + midX
		g.worldObjects[idx].bounds.x2 = ((x2 * SuperCos(radAngle1) - y2 * SuperSin(radAngle1))) + midX
		g.worldObjects[idx].bounds.y1 = ((y1 * SuperCos(radAngle1) + x1 * SuperSin(radAngle1))) + midY
		g.worldObjects[idx].bounds.y2 = ((y2 * SuperCos(radAngle1) + x2 * SuperSin(radAngle1))) + midY
		
		
	}
}


func (g *Game) DrawSprite(screen *ebiten.Image, texture *Texture, distance float64) {
	var wallX, side, rayDirX, rayDirY float64
	var txmin, txmax, tymin, tymax float64
	var size float64

	var texWidth int = texture.w
	var texHeight int = texture.h

	rayDirX = SuperSin(g.player.player_angle)
	rayDirY = SuperCos(g.player.player_angle)

	var x1, x2, y1, y2 = g.hitWall.position.x1, g.hitWall.position.x2, g.hitWall.position.y1, g.hitWall.position.y1
	var hitWallLength = math.Sqrt(math.Pow(x2 - x1, 2) + math.Pow(y2 - y1, 2)) * SCALE

	txmin = (x1 - g.player.px) / rayDirX
	tymin = (y1 - g.player.py) / rayDirY
	txmax = (x2 - g.player.px) / rayDirX
	tymax = (y2 - g.player.py) / rayDirY
	if distance == txmin { // left wall
		side = 0
	}
	if distance == txmax { // right wall
		side = 0
	}
	if distance == tymin { // top wall
		side = 1
	}
	if distance == tymax { // bottom wall
		side = 1
	}
	if side == 0 {
		size = (y2 - y1)
		wallX = (g.player.py + rayDirY * distance)
		wallX = ( wallX - size)
	} else {
		size = (x2 - x1)
		wallX = (g.player.px + rayDirX * distance)
		wallX = ( wallX - size)
	}
	wallX /= size

	println(wallX, hitWallLength)

	var texX int = int(math.Abs((wallX / hitWallLength) * float64(texWidth))) % texWidth

	var lineHeight = int((SCREEN_HEIGHT / distance) * SCALE)
	var drawStart = (-lineHeight / 2 + SCREEN_HEIGHT / 2) + g.player.vov
	var step float64 = float64(texHeight) / float64(lineHeight);
	var texPos float64 = 0

	for idx := 0; idx < lineHeight; idx++ {
		var y = drawStart + idx
		var texY int = int(texPos)
		texPos += step;
		var c color.Color = texture.pixels[texY][texX]
		if side == 1 {
			var r, g, b, a = c.RGBA()
			c = color.RGBA{uint8(r) / 2, uint8(g) / 2, uint8(b) / 2, uint8(a)}
		}

		var i = g.step + y * SCREEN_WIDTH
		if (i < 0) || (i * 4 + 3 > len(buffer_pixels) - 4){
			continue;
		} 
		var r, g, b, a = c.RGBA()
		buffer_pixels[i*4] = byte(r)
		buffer_pixels[i*4+1] = byte(g)
		buffer_pixels[i*4+2] = byte(b)
		buffer_pixels[i*4+3] = byte(a)

	}
}

func (g *Game) DDrawSprite(screen *ebiten.Image, spriteFrame *SpriteAnimationFrame, l Vector) {
	op := &ebiten.DrawImageOptions{}
	// moves image to middle
	op.GeoM.Translate(-float64(spriteFrame.frameWidth)/2, -float64(spriteFrame.frameHeight)/2)
	// where to put it
	op.GeoM.Translate(l.x, l.y)
	sx := spriteFrame.frameOX
	sy := spriteFrame.frameOY
	screen.DrawImage(spriteFrame.spriteSheet.sprite.SubImage(image.Rect(sx, sy, sx+spriteFrame.frameWidth, sy+spriteFrame.frameHeight)).(*ebiten.Image), op)
}

func DrawWorldSprites(g *Game, screen *ebiten.Image) {
	// for _, obj := range g.worldObjects {
	// 	g.DDrawSprite(screen, &obj.spriteData.animationFrames[0], obj.postion)
	// }
}

func DrawGameAnimations(g *Game, screen *ebiten.Image) {
	for _, spriteAnimation := range g.spriteAnimations {
		switch spriteAnimation.state {
			case 0: {
				sprite := spriteAnimation.animationFrames[spriteAnimation.currentFrameNum]
	
				op := &ebiten.DrawImageOptions{}
				op.GeoM.Translate(-float64(sprite.frameWidth)/2, -float64(sprite.frameHeight)/2)
				op.GeoM.Translate(SCREEN_WIDTH/2, SCREEN_HEIGHT - float64(sprite.frameHeight) / 2)
				sx, sy := sprite.frameOX, sprite.frameOY
				screen.DrawImage(
					sprite.spriteSheet.sprite.SubImage(
						image.Rect(
							sx, sy,
							sx+sprite.frameWidth,
							sy+sprite.frameHeight,
							)).(*ebiten.Image), op)
			}
			case 1: {
				handleAnimationState(&spriteAnimation, 0)
				playAnimation(g, screen, &spriteAnimation)
			}
			case 2: {
				continue
			}
		}
	}
}

func (g *Game) DrawSprites(screen *ebiten.Image) {
	// DrawWorldSprites(g, screen)
	DrawGameAnimations(g, screen)
}

func loadMiscSpriteSheet(game *Game) {
	var image, _, err1 = ebitenutil.NewImageFromFile("assets/misc.png")
	var barrelSprite, _, err2 = ebitenutil.NewImageFromFile("assets/barrel.png")
	if err1 != nil { log.Fatal(err1) }
	if err2 != nil { log.Fatal(err2) }
	spriteSheet := Sprite{ id: "MISC", sprite: image }
	game.sprites["MISC"] = spriteSheet
	addGunRevolverSpriteData(game, &spriteSheet)
	addKnifeSpriteData(game, &spriteSheet)
	addGunRevolverItemSpriteData(
		game, 
		&Sprite{id: "BARREL", sprite: barrelSprite, texture: &game.textures[8]},
		Point{SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2},
	)
}

func (g *Game) addWorldObjects()  {
	loadMiscSpriteSheet(g);
}
