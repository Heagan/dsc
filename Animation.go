package main

import (
	"image"
	// "log"
	"time"

	"github.com/hajimehoshi/ebiten/v2"
)

func playAnimation(g *Game, screen *ebiten.Image, spriteAnimation *SpriteAnimation) {
	sprite := spriteAnimation.animationFrames[spriteAnimation.currentFrameNum]

	op := &ebiten.DrawImageOptions{}
	op.GeoM.Translate(-float64(sprite.frameWidth)/2, -float64(sprite.frameHeight)/2)
	op.GeoM.Translate(SCREEN_WIDTH/2, SCREEN_HEIGHT - float64(sprite.frameHeight) / 2)
	sx, sy := sprite.frameOX, sprite.frameOY
	screen.DrawImage(sprite.spriteSheet.sprite.SubImage(image.Rect(sx, sy, sx+sprite.frameWidth, sy+sprite.frameHeight)).(*ebiten.Image), op)
}

func handleAnimationState(spriteAnimation *SpriteAnimation, desiredState int) {
	t := time.Now()
	d := spriteAnimation.duration
	diff := float32(time.Since(spriteAnimation.timeStart).Seconds())
	switch desiredState {
		case 0: {
			if diff > d {
				spriteAnimation.state = 0
				spriteAnimation.currentFrameNum = 0
			} else {
				spriteAnimation.currentFrameNum = int(float32(len(spriteAnimation.animationFrames)) * (diff / d) )
			}
		}
		case 1: {
			if spriteAnimation.state != 10 {
				spriteAnimation.currentFrameNum = 0
				spriteAnimation.state = 1
				spriteAnimation.timeStart = t
			}
		}
	}
}

func addGunRevolverItemSpriteData(game *Game, spriteSheet *Sprite, location Point) {
	frame1 := SpriteAnimationFrame{
		spriteSheet: spriteSheet,
		frameOX: 0,
		frameOY: 0,
		frameWidth: 128,
		frameHeight: 128,
	}
	spriteAnimation := SpriteAnimation{
		id: "REVOLVER_OBJECT",
		spriteSheet: spriteSheet,
		currentFrameNum: 0,
		animationFrames: []SpriteAnimationFrame{ frame1 },
		duration: 0,
		state: 2,
	}

	// spriteData := spriteAnimation
	// var texWidth float64 = float64(spriteData.animationFrames[spriteData.currentFrameNum].frameWidth) / SCALE
	// var texHeight float64 = float64(spriteData.animationFrames[spriteData.currentFrameNum].frameHeight) / SCALE
	
	var size float64 = 2
	x1 := location.x - size
	y1 := location.y - size
	x2 := location.x + size
	y2 := location.y + size

	ws := WorldSprite{
		spriteData: spriteAnimation,
		location: location,
		bounds: Position{x1, y1, x2, y2},
	}
	game.worldObjects = append(game.worldObjects, ws)
}

func addGunRevolverSpriteData(game *Game, spriteSheet *Sprite) {
	frame1 := SpriteAnimationFrame{
		spriteSheet: spriteSheet,
		frameOX: 258,
		frameOY: 903,
		frameWidth: 128,
		frameHeight: 128,
	}
	frame2 := SpriteAnimationFrame{
		spriteSheet: spriteSheet,
		frameOX: 258 + 128 + 1,
		frameOY: 903,
		frameWidth: 128,
		frameHeight: 128,
	}
	frame3 := SpriteAnimationFrame{
		spriteSheet: spriteSheet,
		frameOX: 258 + 128 + 128 + 2,
		frameOY: 903,
		frameWidth: 128,
		frameHeight: 128,
	}

	spriteAnimation := SpriteAnimation{
		id: "REVOLVER",
		spriteSheet: spriteSheet,
		currentFrameNum: 0,
		animationFrames: []SpriteAnimationFrame{ frame1, frame2, frame3 },
		duration: 0.5,
		state: 2,
	}
	game.spriteAnimations = append(game.spriteAnimations, spriteAnimation)
}

func addKnifeSpriteData(game *Game, spriteSheet *Sprite) {
	
	frame1 := SpriteAnimationFrame{
		spriteSheet: spriteSheet,
		frameOX: 0,
		frameOY: 1032,
		frameWidth: 128,
		frameHeight: 128,
	}
	frame2 := SpriteAnimationFrame{
		spriteSheet: spriteSheet,
		frameOX: 645,
		frameOY: 903,
		frameWidth: 128,
		frameHeight: 128,
	}
	frame3 := SpriteAnimationFrame{
		spriteSheet: spriteSheet,
		frameOX: 128 + 1,
		frameOY: 1032,
		frameWidth: 128,
		frameHeight: 128,
	}
	frame4 := SpriteAnimationFrame{
		spriteSheet: spriteSheet,
		frameOX: 128 + 128 + 2,
		frameOY: 1032,
		frameWidth: 128,
		frameHeight: 128,
	}

	spriteAnimation := SpriteAnimation{
		id: "KNIFE",
		spriteSheet: spriteSheet,
		currentFrameNum: 0,
		animationFrames: []SpriteAnimationFrame{ frame1, frame2, frame3, frame4 },
		duration: 0.9,
		state: 2,
	}
	game.spriteAnimations = append(game.spriteAnimations, spriteAnimation)
}

