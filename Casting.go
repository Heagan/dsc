package main

import "math"

func newIntersect (g *Game, p Position) float64 {
	dx := SuperSin(g.player.player_angle)
	dy := SuperCos(g.player.player_angle)

	v1 := Vector{ g.player.px - p.x1, g.player.py - p.y1, 0 }
	v2 := Vector{ p.x2 - p.x1, p.y2 - p.y1, 0 }
	var v3 Vector

	v3 = Vector{-dy, dx, 0}


    var dot float64 = v2.Dot( v3 );
	if math.Abs(dot) < 0.000001 {
		return -1
	}

    var t1 float64 = v2.TWODCross( v1 ) / dot;
    var t2 float64 = v1.Dot( v3 ) / dot;
	
	if t1 >= 0 && t2 >= 0 && t2 <= 1 {
		return t1
	} else {
		return -1
	}
}

func intersect (g *Game, p Position) float64 {
	var rayDirX, rayDirY, t, tmin, tmax, txmin, txmax, tymin, tymax float64
	rayDirX = SuperSin(g.player.player_angle)
	rayDirY = SuperCos(g.player.player_angle)

	txmin = (p.x1 - g.player.px) / rayDirX
	tymin = (p.y1 - g.player.py) / rayDirY
	txmax = (p.x2 - g.player.px) / rayDirX
	tymax = (p.y2 - g.player.py) / rayDirY

	if (txmin > txmax) {
		t = txmin
		txmin = txmax
		txmax = t
	}
	if (tymin > tymax) {
		t = tymin
		tymin = tymax
		tymax = t
	}

	if (txmin > tymax) || (tymin > txmax) {
		return 0
	}
	tmin = txmin
	tmax = txmax
	if (tymin > txmin) {
		tmin = tymin
	}
	if (tymax > txmax) {
		tmax = tymax
	}
	if (tmax < tmin) {
		t = tmax
	} else {
		t = tmin
	}
	return t
}

func (g *Game) collision () float64 {
	var closestDistance, distance float64

	for _, wall := range g.walls {
		distance = intersect(g, wall.position)
		if distance > 0 {
			if distance < closestDistance || closestDistance == 0 {
				closestDistance = distance
				g.hitWall = wall
			}
		}
	}
	return closestDistance
}

func (g *Game) castRays () {
	var distance float64
	var rayStep float64
	// var dis float64
	// spriteData := g.worldObjects[0].spriteData
	// var texWidth float64 = float64(spriteData.animationFrames[spriteData.currentFrameNum].frameWidth) / SCALE
	// var texHeight float64 = float64(spriteData.animationFrames[spriteData.currentFrameNum].frameHeight) / SCALE
	// g.worldObjects[0].position = Position{100, 100 + texWidth, 100, 100 + texHeight}

	g.player.player_angle = g.player.pov - (g.player.fov / 2)
	rayStep = float64(g.player.fov / SCREEN_WIDTH)
	for step := 0; step < SCREEN_WIDTH; step++ {
		g.step = step
		distance = g.collision()
		if (distance > 0) {
			g.wallDistances[g.step] = distance
			g.drawTexturedWall(distance)
		}

		// dis = newIntersect(g, g.worldObjects[0].bounds)
		// if dis > 0 && dis < distance {
		// 	// g.wallDistances[g.step] = distance
		// 	// g.drawTexturedWall(dis)
		// 	// &g.worldObjects[0]
		// 	// g.hitWall.texture = g.worldObjects[0].spriteData.spriteSheet.texture
		// 	g.drawTexturedWall(dis)
		// }
		g.player.player_angle += rayStep
	}
}


func (g *Game) worldObjectsIntersect () {
	// var x = g.worldObjects[0].location.x
	// var y = g.worldObjects[0].location.y
	// var px = g.player.px
	// var py = g.player.py
	// var distance = math.Sqrt(math.Pow(x - px, 2) + math.Pow(y - py, 2))

	// var texture *Texture = g.worldObjects[0].spriteData.spriteSheet.texture
	// var texWidth int = texture.w
	// var texHeight int = texture.h
	// var lineHeight = int((SCREEN_HEIGHT / distance) * (SCALE / 2)) 
	// var drawStart = (-lineHeight / 2 + SCREEN_HEIGHT / 2) + g.player.vov
	// var step float64 = float64(texHeight) / float64(lineHeight);
	// var texPos float64 = 0

	// for idx := 0; idx < lineHeight; idx++ {
	// 	var y = drawStart + idx
	// 	var texY int = int(texPos)
	// 	texPos += step;

	// 	var i = g.step + y * SCREEN_WIDTH
	
	// 	if (i < 0) || (i * 4 + 3 > len(buffer_pixels) - 4) {
	// 		continue;
	// 	} 
	// 	// var c color.Color = texture.pixels[texY][texX]
	// 	var r, g, b, a = c.RGBA()
	// 	buffer_pixels[i*4] = byte(r)
	// 	buffer_pixels[i*4+1] = byte(g)
	// 	buffer_pixels[i*4+2] = byte(b)
	// 	buffer_pixels[i*4+3] = byte(a)

	// }
	// g.step

}