package main

import (
	"image/color"
	_ "image/png"
	"log"
	"math"
	"time"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

func loadImage(image *ebiten.Image) [][]color.Color {
	var texWidth, texHeight = image.Size()
	var pixels = make([][]color.Color, texHeight)

	for y := 0; y < texHeight; y++ {
		pixels[y] = make([]color.Color, texWidth) 
		for x := 0; x < texWidth; x++ {
			pixels[y][x] = image.At(x, y)
		
		}
	}
	return pixels
}

func (g *Game) loadTextures() {
	print("Loading Textures: ")
	texture_filenames := [9]string {
		"assets/1.png",
		"assets/2.png",
		"assets/3.png",
		"assets/4.png",
		"assets/5.png",
		"assets/6.png",
		"assets/7.png",
		"assets/8.png",
		"assets/barrel.png",
	}
	for idx, file := range texture_filenames {
		var image, _, err = ebitenutil.NewImageFromFile(file)
		if err != nil {
			log.Fatal(err)
		}
		var pixels = loadImage(image)
		var w, h = image.Size()
		g.textures[idx] = Texture{
			w: w,
			h: h,
			pixels: pixels,
		}
	}
	println("DONE")
}


func (g *Game) buildWorld () {
	g.loadTextures()
	var size float64 = SCREEN_WIDTH / SCALE

	for xidx, row := range worldMap {
		for yidx, value := range row {
			var x1 = float64(xidx) * size
			var x2 = (x1 + size)
			var y1 = float64(yidx) * size
			var y2 = (y1 + size)
			if value > 0 {
				var pixels = &g.textures[value - 1]
				g.walls = append(g.walls, Wall{
						position: Position{x1, y1, x2, y2},
						texture: pixels,
					},
				)
			}
		}
	}
}

func (g *Game) handleMovement () {
	var mx, my int = ebiten.CursorPosition()
	diff := float64(time.Since(g.tick).Seconds())
	mvSpd := MOVESPD * diff
	rotSpd := 0.1//mvSpd / 4

	if int(math.Abs(float64(g.mx - mx))) > 5 {
		g.player.pov += float64(mx - g.mx) / 2
		g.mx = mx
	}
	if int(math.Abs(float64(g.my - my))) > 5 {
		limits := 0//135
		g.player.vov += -(my - g.my) 
		if g.player.vov > limits {
			g.player.vov = limits
		}
		if g.player.vov < -limits {
			g.player.vov = -limits
		}
		g.my = my
	}
	if ebiten.IsKeyPressed(ebiten.KeyShift) {
		mvSpd *= 2	
	}
	if ebiten.IsKeyPressed(ebiten.KeySpace) || ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) {
		handleAnimationState(&g.spriteAnimations[0], 1)
	}
	if ebiten.IsKeyPressed(ebiten.KeyW) || ebiten.IsKeyPressed(ebiten.KeyUp) {
		g.player.px += SuperSin(g.player.pov) * mvSpd
		g.player.py += SuperCos(g.player.pov) * mvSpd
	}
	if ebiten.IsKeyPressed(ebiten.KeyS) || ebiten.IsKeyPressed(ebiten.KeyDown) { 
		g.player.px += -SuperSin(g.player.pov) * mvSpd
		g.player.py += -SuperCos(g.player.pov) * mvSpd
	}
	if ebiten.IsKeyPressed(ebiten.KeyA) || ebiten.IsKeyPressed(ebiten.KeyLeft) { 
		g.player.px += -SuperSin(g.player.pov + 90) * rotSpd
		g.player.py += -SuperCos(g.player.pov + 90) * rotSpd
	
		oldPlaneX := g.planeX;
		g.planeX = g.planeX  * SuperCos(-rotSpd) - g.planeY * SuperSin(-rotSpd);
		g.planeY = oldPlaneX * SuperSin(-rotSpd) + g.planeY * SuperCos(-rotSpd);
	}
	if ebiten.IsKeyPressed(ebiten.KeyD) || ebiten.IsKeyPressed(ebiten.KeyRight) {
		g.player.px += SuperSin(g.player.pov + 90) * rotSpd
		g.player.py += SuperCos(g.player.pov + 90) * rotSpd 

		oldPlaneX := g.planeX;
		g.planeX = g.planeX  * SuperCos(rotSpd) - g.planeY * SuperSin(rotSpd);
		g.planeY = oldPlaneX * SuperSin(rotSpd) + g.planeY * SuperCos(rotSpd);
	}
	if (g.player.pov > 360) {
		g.player.pov = TURNRATE * diff;
	} else if (g.player.pov < 0) {
		g.player.pov = 360 - TURNRATE * diff;
	}
}