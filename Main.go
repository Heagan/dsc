package main

import (
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"

	_ "net/http"
	_ "net/http/pprof")



func (g *Game) DoOnce() {
	if g.doOnce {
		return
	}
	g.doOnce = true
	g.buildWorld()
	g.addWorldObjects()
}

func (g *Game) Update() error {
	g.DoOnce()
	g.handleMovement()
	g.castRays()
	g.UpdateSpriteRotations()
	if ebiten.IsKeyPressed(ebiten.KeyEscape) {
		return errors.New("game ended by player")
	}
	g.tick = time.Now()
    return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	screen.ReplacePixels(buffer_pixels)
	g.DrawSprites(screen)
	ebitenutil.DebugPrintAt(screen, fmt.Sprintf("TPS: %0.2f", ebiten.CurrentTPS()), 4, 4)
	ebitenutil.DebugPrintAt(screen, fmt.Sprintf("FPS: %0.2f", ebiten.CurrentFPS()), 4, 16)
	ebitenutil.DebugPrintAt(screen, fmt.Sprintf("POS: %0.2f %0.2f POV: %0.02f", g.player.px, g.player.py, g.player.player_angle), 4, 28)
	ebitenutil.DebugPrintAt(
		screen, 
		fmt.Sprintf(
			"POS: %0.2f %0.2f | %0.02f %0.02f", 
			g.worldObjects[0].bounds.x1,
			g.worldObjects[0].bounds.y1,
			g.worldObjects[0].bounds.x2,
			g.worldObjects[0].bounds.y2,
		), 4, 28 + 12)
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return SCREEN_WIDTH, SCREEN_HEIGHT
}


func main() {
	println("Game Starting")
	game := &Game{
		player: Player{
			px: 165,
			py: 165,
			fov: 35.0,
			pov: 10.2,
			player_angle: 0.0,
		},
		sprites: make(map[string]Sprite),
		planeX: 0,
		planeY: 0.66,
	}
	ebiten.SetMaxTPS(60)
	ebiten.SetCursorMode(ebiten.CursorModeCaptured)
	ebiten.SetFullscreen(false)
	ebiten.SetWindowSize(SCREEN_WIDTH * 2, SCREEN_HEIGHT* 2)
    ebiten.SetWindowTitle("Wolf3D")
    if err := ebiten.RunGame(game); err != nil {
        log.Fatal(err)
    }
}




