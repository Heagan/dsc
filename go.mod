module wolf3d

go 1.15

require (
	github.com/hajimehoshi/ebiten v1.12.11 // indirect
	github.com/hajimehoshi/ebiten/v2 v2.0.6
	github.com/ramya-rao-a/go-outline v0.0.0-20200117021646-2a048b4510eb // indirect
	github.com/uudashr/gopkgs/v2 v2.1.2 // indirect
	golang.org/x/tools/gopls v0.6.9 // indirect
	gonum.org/v1/gonum v0.9.1 // indirect
)
