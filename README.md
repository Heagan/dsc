# Golang Wolfenstein Clone 

Remade a bit of the Wolfenstein game for fun to learn Golang 

# Project Progress

Making Walls
![](progress/first.gif)

Texturing Walls
![](progress/second.gif)

Finished World
![](progress/show.gif)

I've added the gun too and want to add enemies and multiplayer next
![](progress/wolf.png)


Some pages I used to help make this:
https://www.scratchapixel.com/lessons/3d-basic-rendering/computing-pixel-coordinates-of-3d-point/mathematics-computing-2d-coordinates-of-3d-points
https://gamedev.stackexchange.com/questions/35263/converting-world-space-coordinate-to-screen-space-coordinate-and-getting-incorre




