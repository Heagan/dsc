package main

import (
	_ "time"
	// "image"
	"image/color"
	"math"

	// "github.com/hajimehoshi/ebiten/v2"
	_ "github.com/hajimehoshi/ebiten/v2"
	_ "github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

func (g *Game) drawTexturedWall(distance float64) {
	var wallX, side, rayDirX, rayDirY float64
	var txmin, txmax, tymin, tymax float64
	var size float64
	
	var texture *Texture = g.hitWall.texture
	var texWidth int = texture.w
	var texHeight int = texture.h

	rayDirX = SuperSin(g.player.player_angle)
	rayDirY = SuperCos(g.player.player_angle)

	var x1, x2, y1, y2 = g.hitWall.position.x1, g.hitWall.position.x2, g.hitWall.position.y1, g.hitWall.position.y2
	
	txmin = (x1 - g.player.px) / rayDirX
	tymin = (y1 - g.player.py) / rayDirY
	txmax = (x2 - g.player.px) / rayDirX
	tymax = (y2 - g.player.py) / rayDirY

	if distance == txmin { // left wall
		side = 0
	}
	if distance == txmax { // right wall
		side = 0
	}
	if distance == tymin { // top wall
		side = 1
	}
	if distance == tymax { // bottom wall
		side = 1
	}
	if side == 0 {
		size = (y2 - y1)
		wallX = (g.player.py + rayDirY * distance)
		wallX = ( wallX - size)
	} else {
		size = (x2 - x1)
		wallX = (g.player.px + rayDirX * distance)
		wallX = ( wallX - size)
	}

	wallX /= size
	var texX int = int(math.Abs(wallX * float64(texWidth))) % texWidth

	var lineHeight = int((SCREEN_HEIGHT / distance) * (SCALE / 2)) 

	var drawStart = (-lineHeight / 2 + SCREEN_HEIGHT / 2) + g.player.vov
	var step float64 = float64(texHeight) / float64(lineHeight);
	var texPos float64 = 0
	
	for y := 0; y < drawStart; y++ {
		var i = g.step + y * SCREEN_WIDTH
		if (i < 0) || (i * 4 + 3 > len(buffer_pixels) - 3){
			continue;
		} 

		minColor := 20.0
		maxColor := 41.0
		color := maxColor - ((maxColor - minColor) * (float64(y) / float64(SCREEN_HEIGHT)))

		buffer_pixels[i*4] = byte(color)
		buffer_pixels[i*4+1] = byte(color)
		buffer_pixels[i*4+2] = byte(color)
		buffer_pixels[i*4+3] = byte(255)
	}

	for idx := 0; idx < lineHeight; idx++ {
		var y = drawStart + idx
		var _ = texX
		var texY int = int(texPos)
		texPos += step;

		var i = g.step + y * SCREEN_WIDTH
	
		if (i < 0) || (i * 4 + 3 > len(buffer_pixels) - 4) {
			continue;
		} 
		

		var c color.Color = texture.pixels[texY][texX]
			
		if side == 1 {
			var r, g, b, a = c.RGBA()
			c = color.RGBA{uint8(r / 2), uint8(g / 2), uint8(b / 2), uint8(a)}
		}

		var r, g, b, a = c.RGBA()
		buffer_pixels[i*4] = byte(r)
		buffer_pixels[i*4+1] = byte(g)
		buffer_pixels[i*4+2] = byte(b)
		buffer_pixels[i*4+3] = byte(a)

	}

	for y := drawStart + lineHeight; y < SCREEN_HEIGHT; y++ {
		var i = g.step + y * SCREEN_WIDTH
		if (i < 0) || (i * 4 + 3 > len(buffer_pixels) - 3) {
			continue;
		} 

		minColor := 80.0
		maxColor := 110.0
		color := minColor + ((maxColor - minColor) * (float64(y - (drawStart + lineHeight)) / (float64(SCREEN_HEIGHT - (drawStart + lineHeight) ) / 2 )))

		buffer_pixels[i*4] = byte(color)
		buffer_pixels[i*4+1] = byte(color)
		buffer_pixels[i*4+2] = byte(color)
		buffer_pixels[i*4+3] = byte(255)
	}

}
