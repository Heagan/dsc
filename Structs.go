package main

import (
	"image/color"
	"time"
	"github.com/hajimehoshi/ebiten/v2"
	_"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

type Position struct {
	x1, y1, x2, y2 float64
}

type Vector struct {
	x, y, z float64
}

type Point struct {
	x, y float64
}

type Texture struct {
	w int
	h int
	pixels [][]color.Color
}

type Sprite struct {
	id string
	sprite *ebiten.Image
	texture *Texture
}

type SpriteAnimationFrame struct {
	spriteSheet *Sprite
	frameOX int
	frameOY int
	frameWidth int
	frameHeight int
}

type SpriteAnimation struct {
	id string
	spriteSheet *Sprite
	animationFrames []SpriteAnimationFrame
	currentFrameNum int
	duration float32
	timeStart time.Time
	state int // 0 - Finished, 1 - ACTIVE, 2 - DISABLED
}

type WorldSprite struct {
	spriteData SpriteAnimation
	location Point
	bounds Position
}

type Wall struct {
	position Position
	texture *Texture
}

type Player struct {
	px, py, player_angle, pov, fov float64
	vov int // Vertical View
	weapon string
}

type Game struct {
	tick time.Time
	doOnce bool
	
	player Player

	step int
	walls []Wall
	wallDistances [SCREEN_WIDTH]float64
	hitWall Wall

	mx, my int
	planeX, planeY float64 

	textures [9]Texture
	sprites map[string]Sprite
	spriteAnimations []SpriteAnimation
	worldObjects []WorldSprite
}
