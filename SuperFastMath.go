package main

import "math"

const (
	cacheStep float64 = 0.01
	factor float64 = math.Pi / 180.0
)
var arrayedSins []float64 = make([]float64, int(360.0 / cacheStep) + 1);
var arrayedCose []float64 = make([]float64, int(360.0 / cacheStep) + 1);


func DEG_TO_RAD(x float64) float64 {
	return (x * math.Pi) / 180
} 

func RAD_TO_DEG(x float64) float64 {
	return x * 180/math.Pi
} 

// func InitAtan2() {
// 	var i int = 0;
// 	for angleDegrees := 0.0; angleDegrees <= 360.0; angleDegrees += cacheStep	{
// 		var angleRadians float64 = angleDegrees * factor;
// 		arrayedSins[i] = math.Atan2(angleRadians);
// 		i++;
// 	}
// }

// func Super2Tan(angleDegrees float64) float64 {	
// 	var index int = int(angleDegrees / cacheStep) - 1;
// 	if index < 0 || index >= len(arrayedSins) {
// 		return math.Sin(angleDegrees * factor)
// 	}
// 	if arrayedSins[index] == 0.0 && arrayedSins[1] == 0.0 {
// 		InitAtan2()
// 	}
// 	return arrayedSins[index];
// }

func InitSin() {
	var i int = 0;
	for angleDegrees := 0.0; angleDegrees <= 360.0; angleDegrees += cacheStep	{
		var angleRadians float64 = angleDegrees * factor;
		arrayedSins[i] = math.Sin(angleRadians);
		i++;
	}
}

func SuperSin(angleDegrees float64) float64 {	
	var index int = int(angleDegrees / cacheStep) - 1;
	if index < 0 || index >= len(arrayedSins) {
		return math.Sin(angleDegrees * factor)
	}
	if arrayedSins[index] == 0.0 && arrayedSins[1] == 0.0 {
		InitSin()
		println("Initilised SIN Lookup Table")
	}
	return arrayedSins[index];
}

func InitCos() {
	var i int = 0;
	for angleDegrees := 0.0; angleDegrees <= 360.0; angleDegrees += cacheStep	{
		var angleRadians float64 = angleDegrees * factor;
		arrayedCose[i] = math.Cos(angleRadians);
		i++;
	}
}

func SuperCos(angleDegrees float64) float64 {	
	var index int = int(angleDegrees / cacheStep) - 1;
	if index < 0 || index >= len(arrayedCose) {
		return math.Cos(angleDegrees * factor)
	}
	if arrayedCose[index] == 0.0 && arrayedCose[1] == 0.0 {
		InitCos()
		println("Initilised COS Lookup Table")
	}
	return arrayedCose[index];
}

func (a Vector) MultiplyByScalar(s float64) Vector {
	return Vector{
	  x: a.x * s,
	  y: a.y * s,
	  z: a.z * s,
	}
}

func (a Vector) Normalize() Vector {
	return a.MultiplyByScalar(1. / a.Length())
}

func (a Vector)Dot(b Vector) float64 {
	return a.x*b.x + a.y*b.y + a.z*b.z
}

func (a Vector) Length() float64 {
	return math.Sqrt(a.Dot(a))
}

func (a Vector) Cross(b Vector) Vector {
	return Vector{
		x: a.y*b.z - a.z*b.y,
		y: a.z*b.x - a.x*b.z,
		z: a.x*b.y - a.y*b.x,
	}
}

func (a Vector) TWODCross(b Vector) float64 {
	return a.x*b.y - a.y*b.x
}


